$(function() {

	$('.js-menu-mobile').on('click', function(){
		$('.page-info__content aside').addClass('opened');
	});

	$('.js-menu-mobile-close').on('click', function(){
		$('.page-info__content aside').removeClass('opened');
	});

});
